/*
|-----------------------------------------------------------------------------|
|                                   ALUNOS                                    |
|                         Gustavo Rodrigues Marques - 6375                    |
|                         Higor de Deus Matos - 6372                          |
|                                                                             |
|-----------------------------------------------------------------------------|
|                                PROFESSOR                                    |
|                            Rodrigo Moreira                                  |
|                      UNIVERSIDADE FEDERAL DE VIÃ‡OSA                        |
|                          CAMPOS RIO PARANAIBA                               |
|-----------------------------------------------------------------------------|
*/

Código feito em C com intuito de colocar em prática o que foi aprendido na aulas de sistemas operacionais.

Utilizado estrutura de laço com while enquanto é pedido para o usuário digitar o comando desejado. Logo após, uma série de funções verifica se o comando é válido, se foi digitado corretamente, 
se ele existe e então executa o mesmo. Quando o comando não pode ser executado uma mensagem de erro é retornada e então é pedido novamente para o usuário digitar um novo.

Um dos maiores problemas que encontramos foi na utilzação dos comandos "fork" e "execvp" já que foi nosso primeiro contato com eles, além de que a validação dos dados digitados pelo usuário foi 
minuciosa para evitar erros e fechamentos do programa brutamente. 

Tivemos dois bugs (conhecidos), mas que não impedem o programa de funcionar livremente: 
O primeiro é na ordem de execução, na qual os comandos podem ser executados em uma ordem que não foi digitada pelo utilizador. 
Obs.: O comando Quit não apresenta o bug citado, impedindo o fechamento indevido do programa.
Já o segundo, que não consideramos exatamente um bug, é o fato de que o comando permanece na string após o usuário ter pressionado a tecla "Enter" e a saída ter sido impressa na tela, fazendo com
que se o usuário, ao voltar para o "meu-shell>", pressionasse a tecla "Enter", sem digitar nenhum comando, tivesse novamente a saída do último comando impressa na tela.
Como forma de resolver tal problema, a solução pensada foi atribuir "espaço" à string (comando_digitado[0] = " ") no início do método main, antes de pedir ao usuário para digitar o comando.
Obs.: A possível solução não foi implementada no código e, por isso, foi adicionada como um bug.

Foram feitos testes individuais e por terceiros na validação de dados, "forçamos" o programa a muitos testes e não encontramos mais erros e nada que atrapalhe ele de prosseguir.
Todos esses exemplos e outros foram testados:

· meu-shell> , cat file , grep foo file2

· meu-shell> cat file , , grep foo file2

· meu-shell> cat file , ls -l ,

· meu-shell> cat file ,,,, ls -l

· meu-shell> ,, ls -l

· meu-shell> ,

· meu-shell> ls -la -ll , df -H , touch file.txt

· meu-shell> quit , poweroff

Ressaltamos que vírgulas e espaços não interferem se o comando for digitado CORRETAMENTE.

Outra dificuldade encontrada foi com o git, não tinhamos experiência nenhuma com ele e "sofremos" um pouco para conseguir utilizar, por isso, com medo de não dar tempo de aprender sobre o assunto 
e atrasar o projeto, versionamos os códigos em bloco de notas e posteriormente utilizamos o git. Hoje temos uma base boa sobre ele e um curso que nos ajudou foi o da Udemy 
https://www.udemy.com/course/git-e-github-para-iniciantes/. 

Utilizaremos o git em nosso próximos projetos pois é uma ferramenta indispensável.
