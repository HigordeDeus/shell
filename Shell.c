
/*
|-----------------------------------------------------------------------------|
|                                   ALUNOS                                    |
|                         Gustavo Rodrigues Marques - 6375                    |
|                         Higor de Deus Matos - 6372                          |
|                                                                             |
|-----------------------------------------------------------------------------|
|                                PROFESSOR                                    |
|                            Rodrigo Moreira                                  |
|                      UNIVERSIDADE FEDERAL DE VIÇOSA                         |
|                          CAMPOS RIO PARANAIBA                               |
|-----------------------------------------------------------------------------|
*/


/******* Área das bibliotecas ******/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
/******* fim das bibliotecas ******/

int comando_invalido; //Variavel global para verificar se o comando foi digitado errado ou invalido
int ls,_bin_ls,ls__l,cat_file,grep_foo_file2;//Variáveis globais para verificar se o comando já foi executado evitando um loop nas mesmas funções.

/******* Área dos protótipos ******/
void verifica_saida(char* comando_digitado);//Verifica se quit foi digitado, sempre a ultima a executar para evitar saida brusca.
void verifica_ls (char* comando_digitado);//Verifica se o comando "ls" foi digitado.
void verifica__bin_ls (char* comando_digitado);//Verifica se o comando "/bin/ls" foi digitado.
void verifica_ls__l (char* comando_digitado);//Verifica se o comando "ls -l" foi digitado.
void verifica_cat_file (char* comando_digitado);//Verifica se o comando "cat file" foi digitado.
void verifica_grep_foo_file2 (char* comando_digitado);//Verifica se o comando "grep foo file2" foi digitado.
void verifica_comando (char* comando_digitado);//Verifica se um comando válido foi digitado, evitando entradas vazias e saidas bruscas.
/******* fim dos protótipos *******/
int main(int argc, char *argv[]){
	char comando_digitado[512];//String para pegar o comando digitado com limite definido.

	do{
		comando_invalido = 0; //Colocando 0 para evitar que o comando invalido apareça na hora indevida.
		
/*Atribuindo 0 monstrando que não foi executado nenhuma vez ainda no programa.*/		
		ls = 0;
		_bin_ls = 0;
		ls__l = 0;
		cat_file = 0;
		grep_foo_file2 = 0;
/* fim da atribuição */
		
		printf("meu-shell> ");
		scanf("%[^\n]s",comando_digitado);//Preferimos a utilização do scanf com o [^\n] para pegar a string completa ao invés do get para evitar mensagens de alerta e problemas.
		setbuf(stdin,NULL);// Lipando o buffer pois com o loop, ocorria um bug ao pegar a string.
		verifica_ls(comando_digitado);
		verifica__bin_ls(comando_digitado);
		verifica_ls__l(comando_digitado);
		verifica_cat_file (comando_digitado);
		verifica_grep_foo_file2 (comando_digitado);
		verifica_saida(comando_digitado);
		verifica_comando(comando_digitado);
			
	}while(1);// Mantendo em loop
	return 0;
}

/******* Verifica QUIT ******/
void verifica_saida(char* comando_digitado){ 
	int i;
	for(i=0;comando_digitado[i] != '\0';i++){
		if(comando_digitado[i] == 'q' && 
		comando_digitado[i+1] == 'u' && 
		comando_digitado[i+2] == 'i' && 
		comando_digitado[i+3] == 't' && 
		(comando_digitado[i+4] == ' ' || comando_digitado[i+4] == '\0') && //Evitar que outro comando seja digitado sem respeitar a virgula e o espaço 
		(i == 0 || comando_digitado[i-1] == ' ') ){//Evitar que outro comando seja digitado sem respeitar a virgula e o espaço, o ''i == 0' mostra que é o primeiro comando do shell digitado evitando erros como "WQquit"
			printf("\n O comando quit foi digitado\n");
			comando_invalido = 1; //Não deixar a mensagem de comando invalido aparecer indevidamente.
			exit(0);
		}	
	}	
	return;
}

/******* Verifica LS ******/
void verifica_ls (char* comando_digitado){
	int i;
	char *args[]={"ls",NULL}; //Comando ls
	for(i=0;comando_digitado[i] != '\0';i++){
		if(comando_digitado[i] == 'l' &&
		 comando_digitado[i+1] == 's' && 
		(comando_digitado[i+2] == ' ' || comando_digitado[i+2] == '\0') &&//Evitar que outro comando seja digitado sem respeitar a virgula e o espaço 
		comando_digitado[i-1] != '/' &&//Evitar confusão com o comando /bin/ls
		comando_digitado[i+3] != '-' &&//Evitar confusão com o comando ls -l
		(i == 0 || comando_digitado[i-1] == ' ') ){//Evitar que outro comando seja digitado sem respeitar a virgula e o espaço, o ''i == 0' mostra que é o primeiro comando do shell digitado evitando erros como "WQquit"

			// Atuação do programa, e condições para concatenação dos demais comandos, ao encontrar 0 signifaca que o comando não foi executado ainda e ao entrar é verificado se foi chamado, ao ecnontrar 1 o comando já foi executado e não deve ser repetido evitando uma infinita repetição das funções chamadas.
			if(fork() == 0){
				execvp(args[0],args);
				ls = 1; //Atribuindo 1 mostrando que o comando já foi executado e não deve ser executado dnv na mesma chamada para evitar um loop.
				if(ls == 0){
					verifica_ls(comando_digitado);
				}
				if(_bin_ls == 0){
					verifica__bin_ls(comando_digitado);
				}
				if(ls__l == 0){
					verifica_ls__l(comando_digitado);
				}
				if(cat_file == 0){
					verifica_cat_file (comando_digitado);
				}
				if(grep_foo_file2 == 0){
					verifica_grep_foo_file2 (comando_digitado);
				}
			}
			else{
				sleep(2);//Esperar o fim do comando
				comando_invalido = 1;//Não deixar a mensagem de comando invalido aparecer indevidamente.
			}
		}
	}			
	return;
}

/******* Verifica /BIN/LS  ******/
void verifica__bin_ls (char* comando_digitado){
	int i;
	char *args[]={"/bin/ls",NULL}; //Comando /bin/ls
	for(i=0;comando_digitado[i] != '\0';i++){
		if(comando_digitado[i] == '/' &&
		comando_digitado[i+1] == 'b' &&
		comando_digitado[i+2] == 'i' && 
		comando_digitado[i+3] == 'n' && 
		comando_digitado[i+4] == '/' && 
		comando_digitado[i+5] == 'l' && 
		comando_digitado[i+6] == 's' &&
		(comando_digitado[i+7] == ' ' || comando_digitado[i+7] == '\0') &&//Evitar que outro comando seja digitado sem respeitar a virgula e o espaço
		(i == 0 || comando_digitado[i-1] == ' ') ){//Evitar que outro comando seja digitado sem respeitar a virgula e o espaço, o ''i == 0' mostra que é o primeiro comando do shell digitado evitando erros como "WQquit"
		
			//Atuação do programa, e condições para concatenação dos demais comandos, ao encontrar 0 signifaca que o comando não foi executado ainda e ao entrar é verificado se foi chamado, ao ecnontrar 1 o comando já foi executado e não deve ser repetido evitando uma infinita repetição das funções chamadas.
			if (fork() == 0){
				execvp(args[0],args);			
				_bin_ls = 1; //Atribuindo 1 mostrando que o comando já foi executado e não deve ser executado dnv na mesma chamada para evitar um loop.
				if(ls == 0){
					verifica_ls(comando_digitado);
				}
				if(_bin_ls == 0){
					verifica__bin_ls(comando_digitado);
				}
				if(ls__l == 0){
					verifica_ls__l(comando_digitado);
				}
				if(cat_file == 0){
					verifica_cat_file (comando_digitado);
				}
				if(grep_foo_file2 == 0){
					verifica_grep_foo_file2 (comando_digitado);
				}
			}
			else{
				sleep(2);//esperar o fim do comando
				comando_invalido = 1;//Não deixar a mensagem de comando invalido aparecer indevidamente.
			}
		}
	}
	return;
}

/******* Verifica LS -L  ******/
void verifica_ls__l (char* comando_digitado){
	int i;
	char *args[]={"ls","-l",NULL}; //Comando ls -l
	for(i=0;comando_digitado[i] != '\0';i++){
		if(comando_digitado[i] == 'l' && 
		comando_digitado[i+1] == 's' && 
		comando_digitado[i+2] == ' ' && 
		comando_digitado[i+3] == '-' && 
		comando_digitado[i+4] == 'l' &&
		(comando_digitado[i+5] == ' ' || comando_digitado[i+5] == '\0') &&//Evitar que outro comando seja digitado sem respeitar a virgula e o espaço) {
		(i == 0 || comando_digitado[i-1] == ' ') ){//Evitar que outro comando seja digitado sem respeitar a virgula e o espaço, o ''i == 0' mostra que é o primeiro comando do shell digitado evitando erros como "WQquit"
		
			//Atuação do programa, e condições para concatenação dos demais comandos, ao encontrar 0 signifaca que o comando não foi executado ainda e ao entrar é verificado se foi chamado, ao ecnontrar 1 o comando já foi executado e não deve ser repetido evitando uma infinita repetição das funções chamadas.
			if (fork() == 0){
				execvp(args[0],args);
				ls__l = 1; //Atribuindo 1 mostrando que o comando já foi executado e não deve ser executado dnv na mesma chamada para evitar um loop.
				if(ls == 0){
					verifica_ls(comando_digitado);
				}
				if(_bin_ls == 0){
					verifica__bin_ls(comando_digitado);
				}
				if(ls__l == 0){
					verifica_ls__l(comando_digitado);
				}
				if(cat_file == 0){
					verifica_cat_file (comando_digitado);
				}
				if(grep_foo_file2 == 0){
					verifica_grep_foo_file2 (comando_digitado);
				}
			}
			else{
				sleep(2);//esperar o fim do ls
				comando_invalido = 1;//Não deixar a mensagem de comando invalido aparecer indevidamente.
			}
		}	
	}	
	return;
}

/******* Verifica cat file  ******/
void verifica_cat_file (char* comando_digitado){
	int i;
	char *args[]={"cat", "file",NULL}; //Comando cat file
	for(i=0;comando_digitado[i] != '\0';i++){
		if(comando_digitado[i] == 'c' && 
		comando_digitado[i+1] == 'a' && 
		comando_digitado[i+2] == 't' && 
		comando_digitado[i+3] == ' ' && 
		comando_digitado[i+4] == 'f' && 
		comando_digitado[i+5] == 'i' && 
		comando_digitado[i+6] == 'l' && 
		comando_digitado[i+7] == 'e' &&
		(comando_digitado[i+8] == ' ' || comando_digitado[i+8] == '\0') &&//Evitar que outro comando seja digitado sem respeitar a virgula e o espaço) {		
		(i == 0 || comando_digitado[i-1] == ' ') ){//Evitar que outro comando seja digitado sem respeitar a virgula e o espaço, o ''i == 0' mostra que é o primeiro comando do shell digitado evitando erros como "WQquit"		
			
			//Atuação do programa, e condições para concatenação dos demais comandos, ao encontrar 0 signifaca que o comando não foi executado ainda e ao entrar é verificado se foi chamado, ao ecnontrar 1 o comando já foi executado e não deve ser repetido evitando uma infinita repetição das funções chamadas.
			if (fork() == 0){
				execvp(args[0],args);
				cat_file = 1; //Atribuindo 1 mostrando que o comando já foi executado e não deve ser executado dnv na mesma chamada para evitar um loop.	
				if(ls == 0){
					verifica_ls(comando_digitado);
				}
				if(_bin_ls == 0){
					verifica__bin_ls(comando_digitado);
				}
				if(ls__l == 0){
					verifica_ls__l(comando_digitado);
				}
				if(cat_file == 0){
					verifica_cat_file (comando_digitado);
				}
				if(grep_foo_file2 == 0){
					verifica_grep_foo_file2 (comando_digitado);
				}
			}
			else{
				sleep(2);//esperar o fim do ls
				comando_invalido = 1;//Não deixar a mensagem de comando invalido aparecer indevidamente.
			}
		}	
	}	
	return;
}

/******* Verifica grep foo file2  ******/
void verifica_grep_foo_file2 (char* comando_digitado){
	int i;
	char *args[]={"grep", "foo", "file2",NULL}; //Comando grep foo file2
	for(i=0;comando_digitado[i] != '\0';i++){
		if(comando_digitado[i] == 'g' &&
		 comando_digitado[i+1] == 'r' && 
		 comando_digitado[i+2] == 'e' &&
		 comando_digitado[i+3] == 'p' && 
		 comando_digitado[i+4] == ' ' && 
		 comando_digitado[i+5] == 'f' && 
		 comando_digitado[i+6] == 'o' && 
		 comando_digitado[i+7] == 'o' && 
		 comando_digitado[i+8] == ' ' && 
		 comando_digitado[i+9] == 'f' && 
		 comando_digitado[i+10] == 'i' && 
		 comando_digitado[i+11] == 'l' && 
		 comando_digitado[i+12] == 'e' && 
		 comando_digitado[i+13] == '2' &&
		(comando_digitado[i+14] == ' ' || comando_digitado[i+14] == '\0') &&//Evitar que outro comando seja digitado sem respeitar a virgula e o espaço) {		
		(i == 0 || comando_digitado[i-1] == ' ') ){//Evitar que outro comando seja digitado sem respeitar a virgula e o espaço, o ''i == 0' mostra que é o primeiro comando do shell digitado evitando erros como "WQquit"				 
			
			//Atuação do programa, e condições para concatenação dos demais comandos, ao encontrar 0 signifaca que o comando não foi executado ainda e ao entrar é verificado se foi chamado, ao ecnontrar 1 o comando já foi executado e não deve ser repetido evitando uma infinita repetição das funções chamadas.
			if (fork() == 0){
				execvp(args[0],args);
				grep_foo_file2 = 1; //Atribuindo 1 mostrando que o comando já foi executado e não deve ser executado dnv na mesma chamada para evitar um loop.
				if(ls == 0){
					verifica_ls(comando_digitado);
				}
				if(_bin_ls == 0){
					verifica__bin_ls(comando_digitado);
				}
				if(ls__l == 0){
					verifica_ls__l(comando_digitado);
				}
				if(cat_file == 0){
					verifica_cat_file (comando_digitado);
				}
				if(grep_foo_file2 == 0){
					verifica_grep_foo_file2 (comando_digitado);
				}
			}
			else{
				sleep(2);//esperar o fim do ls
				comando_invalido = 1;//Não deixar a mensagem de comando invalido aparecer indevidamente.
			}
		}	
	}	
	return;
}

/******* Verifica o comando digitado  ******/
void verifica_comando (char* comando_digitado){
	if(comando_invalido == 0){ // Verifica se a variável comando_invalido continua 0 sendo assim nenhum comando válido foi digitado.
		printf("\nComando inexistente ou impossivel de ser executado\n");
	}
}

//Fim do programa


